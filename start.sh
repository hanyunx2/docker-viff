#!/bin/bash
export PYTHONPATH=$PYTHONPATH:$HOME/opt/lib/python

sudo ln -s /viff-f1d477e94d0b/viff/ /usr/lib/python2.7/dist-packages/viff
cd /viff-f1d477e94d0b/apps/
tmux new-session 'python generate-config-files.py -n 3 -t 1 localhost:9001 localhost:9002 localhost:9003' \; \
	splitw -h -p 50 'python millionaires.py --no-ssl player-1.ini; bash' \; \
	splitw -v -p 50 'python millionaires.py --no-ssl player-2.ini; bash' \; \
	selectp -t 0 \; \
	splitw -v -p 50 'python millionaires.py --no-ssl player-3.ini; bash'