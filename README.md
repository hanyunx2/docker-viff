# docker-Viff
Docker files for installing [viff](http://www.viff.dk), with a demo of the **millionaires example**.

## Getting started
```
docker build -t ubuntu:xx .
docker run -dit ubuntu:xx
docker exec -it <container ID> /bin/bash

```

## Notes on docker
```
docker images
docker ps
docker ps -a
docker container ls

```

