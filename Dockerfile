FROM ubuntu:14.04
RUN apt-get update \
    && apt-get -y install git \
    && apt-get -y install wget \
    && apt-get -y install tmux \
    && apt-get -y install python-pip \
    && apt-get -y install python-dev \
    && apt-get -y install python-twisted-core \
    && apt-get -y install python-gmpy \
    && wget http://hg.viff.dk/viff/archive/tip.tar.gz \
    && tar -xvf tip.tar.gz \
    && cd viff-f1d477e94d0b/ \
    && python setup.py install --home=$HOME/opt

COPY ./start.sh /
#CMD sh /start.sh
CMD bash -C '/start.sh';'bash'